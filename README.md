This is my starter kit for making some Html templates or landing pages faster and easy. 

I already have included the necessary things and tools to start developing the project, like:

```bash
 - bootstrap
 - sass 
 - jQuery 
 - Nunjucks templating engine
 - browserSync
```
## Installation


```bash
npm install
```
It will install all required packages to the node_modules folder.

## Usage

You have to run the following command to enable the watcher and compile the project:
```python
gulp
```
The 'dist' folder will appear in the root.
