var gulp        = require('gulp');
var nunjucks    = require('gulp-nunjucks');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var del         = require('del');
var uglify = require('gulp-uglify');

/* Compile Sass */
gulp.task('sass', function(done) {
    gulp.src("app/scss/**/*.scss")
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());

    done();
});


/* Compile JS */
gulp.task('js', function(done) {
  
    gulp.src([
	   
	    'node_modules/jquery/dist/jquery.min.js',
	    'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
	    './app/js/main.js'
	    // Other scripts
  	])
  	.pipe(concat('lib.js'))
    .pipe(uglify())
  	.pipe(gulp.dest('dist/js'))
	.pipe(browserSync.stream());
    done();
});

/* Compile Html */
gulp.task('html', function(done) {
    return gulp.src("app/**/*.html")
        .pipe(nunjucks.compile())
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream());

    done();
});

/* Move fonts to dist */
gulp.task('fonts', function(done) {
    gulp.src('app/fonts/**/*')
     	.pipe(gulp.dest('dist/fonts'))

    done();
});

/* Move images to dist */
gulp.task('images', function(done){
    gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
  		.pipe(gulp.dest('dist/images'))

  	done();
});

/* Clean up the dist folder */
gulp.task('clean', function(){
     return del('dist/partials/**', {force:true});
});

/* Run the watcher */ 
gulp.task('serve', function(done) {

    browserSync.init({
        server: "dist/"
    });

    gulp.watch("app/scss/**/*.scss", gulp.series('sass'));
    gulp.watch("app/js/*.js", gulp.series('js'));
   
    gulp.watch("app/**/*.html").on('change', () => {
      gulp.src("app/*.html")
          .pipe(nunjucks.compile({name: 'Sindre'}))
          .pipe(gulp.dest('dist'))
     	  browserSync.reload();

      done();
    });
    

    done();
});

gulp.task('default', gulp.series('sass', 'html', 'js', 'fonts', 'images', 'clean', 'serve'));